function initMIDIPlayer(soundfontUrl){
	soundfontUrl = soundfontUrl || "./bower_components/llmidi/soundfont/";
	MIDI.loadPlugin({
		soundfontUrl: soundfontUrl,
		callback: function() {
			MIDI.delay_ = 0; // play one note every quarter second
			MIDI.velocity_ = 127; // how hard the note hits
		}
	});
};

function playNote(note){
	note = note.replace('/','');
	MIDI.setVolume(0, 512);
	note = MIDI.keyToNote[note];
	MIDI.noteOn(0, note, MIDI.velocity_, MIDI.delay_);
	MIDI.noteOff(0, note, MIDI.delay_ + 0.75);
};


function playNotesSeq(notes){
  var playlength;
  var note;
  len = notes.length;
  MIDI.setVolume(0, 512);
  note = MIDI.keyToNote[note];
  (function playStuff(i) {
    if (i) {
      note = MIDI.keyToNote[notes[len - i].note];
      switch (notes[len - i].playlength){
        case 'f': playlength=1*0.1875*10000; break;
        case 'h': playlength=1/2*0.1875*10000; break;
        case 'q': playlength=1/4*0.1875*10000; break;
        default:
          return false;
      }
      console.log(note);
      MIDI.noteOn(0, note, MIDI.velocity_,MIDI.delay_);
      setTimeout(function() { playStuff(--i); }, playlength);
    }
  })(len);

}
